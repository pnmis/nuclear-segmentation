# -*- coding: cp1250 -*-
from Imports import *

import globs

from module_a04 import main as prerecognition
from module_b06 import main as contour
from module_b06 import polar,cartesian
from module_a04 import gaussian_grid,GAUSSIAN
import tkMessageBox
from tkFileDialog import askopenfilename
from PIL import IptcImagePlugin as picinfo
import extensions2 as tiffextensions

two_pi=2*pi
theta_step=globs.theta_step
font2 = ImageFont.truetype("arial.ttf", 30)
output_block_margin=0

def find_closest(seq,value):
    theta_index=int(value/theta_step)
    return(seq[theta_index])

def frame_cutting(plane,origin,block_range,offset=[0,0]):
    y_m=origin[0]+offset[1]
    x_m=origin[1]+offset[0]
    print "offset", offset
    print "origin",x_m,y_m
    cut=plane[y_m-block_range:y_m+block_range,x_m-block_range:x_m+block_range]
    new_origin=(y_m,x_m)
    return (cut,new_origin)
   



def list_size(lst):
   elements=0
   for item in lst:
       if (type(item)==ListType):
           elements=elements+list_size(item)
       else:
           elements=elements+1
   return (elements)
            
        

def show_image(root,im,text=None):
    #img=scipy.misc.toimage(cut,  mode="L")
    #im=img.convert("RGB")
    global im_counter
    tkIm = ImageTk.PhotoImage(im)
    label_image = tk.Label(root, image=tkIm)
    label_image.place(x=0,y=0,width=im.size[0],height=im.size[1])
    if (text!=None):
        label_text = tk.Label(root, text=text)
        label_text.place(x=im.size[0]+20,height=im.size[1]/2)
        label_text.config(bg='black', fg='yellow')
        
    print str(im_counter)+"_"+str(current_plane+1000)[1:]+".tif"+" saved"
    im.save("c:\\biophysics\\out2\\"+str(im_counter)+"_"+str(current_plane+1000)[1:]+".tif","TIFF")
    
    
    root.update()


def random_vector(n):
        v_comp=[]
        for i in range(n):
                v_comp.append(random.uniform(0,1))
        v_norm=sqrt(sum(multiply(v_comp,v_comp)))
        return(divide(v_comp,v_norm))

def maximum_index(seq,index):
    beg_value=seq[0][index]
    ind=0
    i=0
    for row in seq:
        #print row[index],beg_value
        if (row[index]>beg_value):
            beg_value=row[index]
            ind=i
        i=i+1
    return (ind)

def side_minimum(seq,max_index,side):
    beg_value=seq[max_index][4]
    if (side>0):
        m_range=range(max_index,len(seq))
    else:
        m_range=range(max_index,-1,-1)
    ind=0
    
    
    for i in m_range:
        row=seq[i]
        
        if (row[4]<beg_value):
            beg_value=row[4]
            ind=i
        else:
           if (row[4]>1.1*beg_value):
               
                break
    
    return (ind)
 

#MAIN PROGRAM

def main(par,msk_file,make_description=False,cut_separately=False,colorize=False,discard_incomplete=True,no_number=False):
    block_range=globs.block_range
    max_depth_planes=globs.max_depth_planes
    head_percentage=globs.head_percentage
    acceptance_percentage=globs.acceptance_percentage
    scale=globs.scale
    im_counter=0

    #par="c:\\biophysics\\test2.tif"
    #print par
    im =Image.open(par)
    print picinfo.getiptcinfo(im)
    
    dir_name,file_name=os.path.split(par)
    print dir_name
    file_beg,file_ext=os.path.splitext(file_name)
    output_path=dir_name+"\\"+file_beg+"_images"
    if (cut_separately==True):
        
        if ((os.path.isdir(output_path)==False)):
            os.mkdir(output_path)
            print output_path
        else:
            tkMessageBox.showwarning("Saving file", "Output directory %s exists, existing images will be overwritten" % output_path )

        mskname_file=open(output_path+"\\default_mask.log", 'w')
        mskname_file.write(msk_file)
        mskname_file.close()
        
    pkl_file = open(msk_file, 'rb')
    #print pkl_file
    nuclei_list= pickle.load(pkl_file)
    print len(nuclei_list)

    ts=time.time()

    if (im.mode=="P"):
        tkMessageBox.showwarning("Indexeed colour image stack can not be accepted.\n Convert the images to RGB colour or grayscale.")
        os._exit(0)

    image_sequence=[]

    print "OPENING IMAGES..."
    frame_index = 0
    try:
      while 1:
        im.seek(frame_index)
        print frame_index
        frame_index = frame_index + 1
        if (im!=None):
            if (im.mode!="RGB"):
                frame=im.convert("RGB")
            else:
                frame=im.convert("RGB")
            
            image_sequence.append(frame)
    except EOFError:
       #print "eof"
       im.seek(0)
       im_size=im.size
       pass

    #make p-c matrix
    #c_p_matrix=[]
    #ts=time.time()
    #for x in range(im_size[0]):
    #    x_row=[]
    #    for y in range(im_size[1]):
    #        x_row.append(polar(x,y))
    #    c_p_matrix.append(x_row)
    #print "time_conv",time.time()-ts
    nucleus_nr=0
    for nucleus in nuclei_list:
        color_vect=random_vector(3)
        nucleus_nr=nucleus_nr+1
        #if (nucleus_nr!=10):
        #    continue
        print "Cutting nucleus %d out of %d" % (nucleus_nr,len(nuclei_list))
        final_cut=[]
        mask_list=nucleus[3]
        print nucleus_nr,nucleus[1],nucleus[2]
        
	#CHECK IF CUTTING ALLOWED
	if ((nucleus[1]=="no_recog")&(nucleus[2]=="no_recog")):
		#accepted, marked green
		contour_color=(0,250,0)
		output_flag=1
	else:
		if (((nucleus[1]=="no_recog")|(nucleus[1]=="picture_end"))&((nucleus[2]=="no_recog")|(nucleus[2]=="picture_end"))):
			#accepted if option out_of_picture, marked yellow
			contour_color=(250,250,0)
			if (discard_incomplete==False):
				
				output_flag=1
			else:
				output_flag=0
		else:
			#rejected
			contour_color=(250,0,0)
			output_flag=0
	
	
    
        contours_list=[]
        cartesian_contours_limits=[]
        #t_0=time.time()
        mask_list.sort(lambda x, y: cmp(x[0],y[0]),reverse=0)
        max_area_index=maximum_index(mask_list,4)
        up_min=side_minimum(mask_list,max_area_index,-1)
        down_min=side_minimum(mask_list,max_area_index,1)
        if ((mask_list[up_min][4]>0.9*mask_list[0][4])&(nucleus[2]=="picture_end")):
            up_min=0
        
        mask_rel_index=0
        #print "TIME",time.time()-t_0
        for mask in mask_list:
            
            plane=mask[0]
            position=mask[1]
            r_contour=mask[2]
            area=mask[4]
	    try:
		    plane_type=mask[5]
	    except:
		    plane_type=None
            frame=image_sequence[plane]
            cartesian_contour=[]
            #print max_area_index,mask_rel_index,up_min,down_min
            if (((mask_rel_index>down_min)&(mask_rel_index>max_area_index))|((mask_rel_index<up_min)&(mask_rel_index<max_area_index))):
                stop_flag=True
                current_contour_color=(250,0,0)
            else:
                stop_flag=False
                
                current_contour_color=contour_color
            mask_rel_index=mask_rel_index+1
            if plane_type==0:
		    current_contour_color=(0,0,255)
	    if no_number==True:
		     current_contour_color=(0,200,255)
	    # number the contour
	    if (make_description):
		    draw = ImageDraw.Draw(frame)
		    if no_number==False:
			draw.text((position[1],position[0]), str(nucleus_nr), fill="yellow",font=font2)
	    
            for i in range(len(r_contour)):
                x_0,y_0=cartesian(r_contour[i],i*theta_step)
                x,y=map(int,[x_0+position[1],y_0+position[0]])
                cartesian_contour.append([x,y])
                
                
                if (make_description):
                    try:
                        frame.putpixel((x,y),current_contour_color)
                        

                    except(IndexError):
                        print "oor"
            x_h,y_h=numpy.max(cartesian_contour,0)
            x_l,y_l=numpy.min(cartesian_contour,0)        
            contours_list.append([plane,[position[1],position[0]],r_contour,stop_flag])
            cartesian_contours_limits.append([x_h,x_l,y_h,y_l])
            
        if ((cut_separately==True)|(colorize==True)):
            
            # RENDERING SINGLE NUCLEUS
            x_max=int(numpy.max(cartesian_contours_limits,0)[0])+output_block_margin
            y_max=int(numpy.max(cartesian_contours_limits,0)[2])+output_block_margin
            x_min=int(numpy.min(cartesian_contours_limits,0)[1])-output_block_margin
            y_min=int(numpy.min(cartesian_contours_limits,0)[3])-output_block_margin
            origin_f=[x_min,y_min]
            block_size=[int(x_max-x_min),int(y_max-y_min)]
            contours_list.sort(lambda x, y: cmp(x[0],y[0]),reverse=0)
            
	    for i in range(len(contours_list)):
                
                contour=contours_list[i][2]
                origin_c=contours_list[i][1]
                plane=contours_list[i][0]
                stop_flag=contours_list[i][3]
                block_frame=(image_sequence[plane]).crop((x_min,y_min,x_max,y_max))
		f_to_c=map(int,subtract(origin_f,origin_c))
                time_cum=0
		
		#cutting the nucleus
		
                if (colorize==False):
                    for x in range(block_size[0]):
                        for y in range(block_size[1]):
                            pos_c=[x+f_to_c[0],y+f_to_c[1]]
                            r,theta=polar(*pos_c)
                            r_edge=find_closest(contour,theta)
                            if ((r>r_edge)|(stop_flag==True)):
                                block_frame.putpixel((x,y),(0,0,0))
                    final_cut.append(block_frame)
                
		#colorize the nucleus
		
		if (colorize==True):
			
                    for x in range(block_size[0]):
                        for y in range(block_size[1]):
                            
                            pos_c=[x+f_to_c[0],y+f_to_c[1]]
                            r,theta=polar(*pos_c)
                            r_edge=find_closest(contour,theta)
                            
                            if ((r<=r_edge)&(stop_flag==False)):
                                x_f=x+origin_f[0]
                                y_f=y+origin_f[1]
                               
                                try:
                                    pix=(image_sequence[plane]).getpixel((x_f,y_f))
                                    (image_sequence[plane]).putpixel((x_f,y_f),(int(pix[0]*color_vect[0]),int(pix[1]*color_vect[1]),int(pix[2]*color_vect[2])))
                               
                                except (IndexError):
                                    oor_flag=1
                    final_cut.append(block_frame)
                
	    #save block
            if ((output_flag==1)&(colorize==False)):
                tiffextensions.saveTiffMultipageFromSeq(final_cut, output_path+"\\"+str(nucleus_nr)+".tif", rescaleSeqTo8bit=False,rgbOrder="RGB")
                
    #END WARNING
    warning_text=""
    if (make_description==True):
        add_to_s="Mask and test image saved in %s." % dir_name
        warning_text=warning_text+add_to_s
    if (cut_separately==True):
        add_to_s="Output stackes saved in %s." % output_path
        warning_text=warning_text+add_to_s
    if (colorize==True):
        add_to_s="Image with colorized nuclei saved in %s." % dir_name
        warning_text=warning_text+add_to_s
        
        
    if (make_description==True):        
        tiffextensions.saveTiffMultipageFromSeq(image_sequence, dir_name+"\\"+file_beg+"_contours.tif", rescaleSeqTo8bit=False,rgbOrder="RGB")
    if (colorize==True):        
        tiffextensions.saveTiffMultipageFromSeq(image_sequence, dir_name+"\\"+file_beg+"_colorized.tif", rescaleSeqTo8bit=False,rgbOrder="RGB")
        
    tkMessageBox.showwarning("Finished!", warning_text )
 

            
        
