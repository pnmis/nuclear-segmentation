from scipy import *
import sys, os
import ConfigParser
two_pi=2*pi

config = ConfigParser.ConfigParser()
pathname = os.path.dirname(sys.argv[0])
configpath=os.path.abspath(pathname)
config.read(configpath+'\\config.cfg')
print configpath
max_depth_planes=int(config.get("z_dimensions", "max_depth"))
min_depth_planes=int(config.get("z_dimensions", "min_depth"))
min_level=double(config.get("contrast", "min_contrast"))
min_separation=double(config.get("xy_dimensions", "min_separation"))
min_diameter=double(config.get("xy_dimensions", "min_diameter"))
max_diameter=double(config.get("xy_dimensions", "max_diameter"))
theta_step=double(config.get("precision","angle_step" ))

#control parameters
use_booster=True
test=False
disttosurf=config.get("other", "dist_to_surface")
if (disttosurf=='True'):
  disttosurf=True
else:
  disttosurf=False

#initial recognition
med_diameter=3.0*(min_diameter+max_diameter)/7.0
remove_margin=0.25*min_diameter
spacing=5
block_range=double(config.get("other","block_range" ))
block_dim=double(config.get("other","block_dim" ))
#block_range=120
#block_dim=100
quadrupole_step=6000.0
dipole_step=2
p_separation=double(min_separation)
   

#z-plane parameters
decrease_asymmetry=double(config.get("precision","decrease_asymmetry" ))
r_offset=int(config.get("other","r_offset"))
interdrift=int(decrease_asymmetry*r_offset)
acceptance_percentage=0.5

# contour finding
decrease_def=4

#acceptance parameters
head_percentage=0.95
drift_coefficient=int(config.get("other","drift_coefficient"))
#pix per radian

drift_constant=int(config.get("other","drift_constant"))
fourier_max=int(config.get("other","fourier_max"))
try:
	max_threshold=int(config.get("other","max_threshold"))
except:
	max_threshold=None
rough_level=1+0.5*(min_level-1)
masktype=int(config.get("other","mask_type" ))

#display parameters
delta=8
scaled=(600,600)
scale=0.4 

#ftp data
address='ftp.drivehq.com'
user_name="nuclei_statistics"
password="nencki"
ftp_dir="PublicFolder"

#other
version="012"
try:
	mask_path=str(config.get("other","mask_path"))
except:
	mask_path=None
#block_range=120

#table organization
description_fields=["Experiment Name","Animal Group","Time Point","Date of Imaging","Animal ID","Stack NR","Red CH", "Green CH", "Blue CH","Min Chromocenter Vol"]
info_fields=["Mask Name","Scale x-y","Scale z","User","Computer"]