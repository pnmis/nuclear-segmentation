from Imports import *
from scipy.ndimage.measurements import maximum_position

import math
from numpy import *
import ols
import globs

delta=3
goodness_out_len=10
goodness_out_in=10
global time_cum
font2 = ImageFont.truetype("arial.ttf", 30)

def optMinGold(f, xi, xf, t=1.0e-9,maxit=15):
    # constants
    A = 0.6180339887
    
    n = 0
    
    x1 = xi
    x2 = xf
    delta_x=x2-x1
    x3 = x1 + A * (x2-x1)
    x4 = x2 - A * (x2-x1)
    
    fx3 = f(x3)
    fx4 = f(x4)
    
    while abs(x2-x1) > t:
        #print n, x1, x2, abs(x2-x1)
        n+=1
	
        if fx3 < fx4:
        # fx4 > fx3
            x1 = x4
            x4 = x3
            x3 = x1 + A * (x2-x1)
	    fx4 = fx3
	    fx3 = f(x3)
	    
        else:
         # fx4 < fx3
            x2 = x3
            x3 = x4
            x4 = x2 - A * (x2-x1)
            fx3 = fx4
            fx4 = f(x4)
        
	#print fx3,fx4
	if maxit<n:
		break
        
    return (x1,x2,n)

def join_array(a,b):
    outcome=[]
    for row in a:
        outcome.append(row)
    for row in b:
        outcome.append(row)
    return(outcome)


def if_contains(seq,el):
    ct=0
    for point in seq:
        if (point[0]==el[0]):
            ct=1
            break
    return(ct)

def fourier_component(support,fun_type,order):
    if (fun_type=="sin"):
        fun=math.sin
    if (fun_type=="cos"):
        fun=math.cos
    
    pnt=len(support)
    int_a=0
    for row in support:
        theta=row[0]
        r=row[1]
        #print r,theta
        int_a=int_a+r*fun(theta*order)
    int_a=int_a*2.0/pnt
    return (int_a)

def gaussian_grid(size = 5):
    """
    Create a square grid of integers of gaussian shape
    e.g. gaussian_grid() returns
    array([[ 1,  4,  7,  4,  1],
           [ 4, 20, 33, 20,  4],
           [ 7, 33, 55, 33,  7],
           [ 4, 20, 33, 20,  4],
           [ 1,  4,  7,  4,  1]])
    """
    m = size/2
    n = m+1  # remember python is 'upto' n in the range below
    x, y = mgrid[-m:n,-m:n]
    # multiply by a factor to get 1 in the corner of the grid
    # ie for a 5x5 grid   fac*exp(-0.5*(2**2 + 2**2)) = 1
    fac = exp(m**2)
    g = fac*exp(-0.5*(x**2 + y**2))
    return g.round().astype(int)

class GAUSSIAN(ImageFilter.BuiltinFilter):
    name = "Gaussian"
    gg = gaussian_grid().flatten().tolist()
    filterargs = (5,5), sum(gg), 0, tuple(gg)



def cartesian(r,theta):
          
          c=math.cos(theta)
          s=math.sin(theta)          
          
          x=int(r*c)
          y=int(r*s)
          
          
          return([x,y])
    

def polar(x,y):
    
    r=math.sqrt(pow(x,2)+pow(y,2))
    if (r!=0):
            theta=angle(x/r,y)
    else:
            theta=0
    
    return([r,theta])

def polar_coordinates_matrix(x_dim,y_dim):
                
          xy=numpy.zeros((x_dim,y_dim,2))
          xm=x_dim/2
          ym=y_dim/2
          for x in range(x_dim):
                for y in range(y_dim):
                    xc=x-x_dim/2
                    yc=y-y_dim/2
                    r=numpy.sqrt(pow(xc,2)+pow(yc,2))
                    if (r!=0):
                        theta=angle(xc/r,yc)
                    else:
                        theta=0
                    xy[x,y]=[r,theta]
           
          return(xy)



def angle(x,y):
    if (y>0):
        angle=math.acos(x)
    else:
        angle=globs.two_pi-math.acos(x)
    return(angle)




def edge_contrast(section,r,maskin,maskout):
            rad=int(r)
            maskinlen=len(maskin)
            maskoutlen=len(maskout)
            mask=numpy.zeros(len(section))
            try:
                mask[rad-maskinlen:rad]=maskin
            except(ValueError):
                print "MASK TOO SMALL"
                print rad,maskinlen
            #mask[rad:rad+maskoutlen]=maskout
            if ((rad+maskoutlen)<=len(section)):
                mask[rad:rad+maskoutlen]=maskout
            else:
                mask[rad:rad+maskoutlen]=maskout[:len(section)-(rad+maskoutlen)]
            return -sum(multiply(mask,section))

def radial_section(theta,f,x_dim,y_dim):
    
    ka=[]
    
    section=[]
    rmin=0
    rmax=x_dim
     
    for r in range(rmin,rmax):
                
                [x,y]=cartesian(r,theta)
                
                #print x,y
                x_rel=y+y_dim_half
                y_rel=x+x_dim_half
                
                
                #print y+y_dim_half,x+x_dim_half
                if ((x_rel<0)|(x_rel>=x_dim)|(y_rel<0)|(y_rel>=y_dim)):
                    #print x_rel,y_rel,r
                    break
                
                try:
                      value=f[int(x_rel)][int(y_rel)]
                except (IndexError):
                      print r,y+y_dim_half,x+x_dim_half
                      print "error"
                
                section.append(value)
                 
      
     
    return(section)




class main:
    
  def __init__(self,a,r_mask=[],decreasing=False):
    ff=globs.masktype
    old_center=[0,0]  
    global time_cum
    print "decreasing",decreasing
    time_cum=0
    time_start=time.time()
    #ts=time.time()
    f=list(a)

    x_dim=len(a)
    y_dim=len(a[0])
    global x_dim_half
    global y_dim_half
    x_dim_half=int(x_dim/2)
    y_dim_half=int(y_dim/2)
    
    img=scipy.misc.toimage(f,  mode="L")
    im=img.convert("RGB")



    maskin=[0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1.5,1.5,2.5,3,4]
    #maskout=[-4,-4,-4,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-0.5,-0.5,-0.5]
    #maskin=numpy.ones(40)
    maskout=[-2.5,-1.5,-1.5,-1.5,-1.5,-1.5,-1.5,-1.5,-1.5,-1.5,-1,-1,-1,-1,-1,-1,-1,-1,-1]

    maskins=[1,1,1,1,1,1,1.5,1.5,1.5,2,4]
    maskouts=[-4,-2,-2,-2,-1,-1]
    #maskin=[1,1,1,1,1,1,1.5,1.5,1.5,2,4]
    #maskout=[-4,-2,-2,-2,-1,-1]

    #global parameters
    
    theta_step=globs.theta_step
    drift_coefficient=globs.drift_coefficient
    drift_constant=globs.drift_constant
    min_drift=5
    fourier_max=5
    min_radius=40
    max_radius_offset=25
    r_offset=globs.r_offset
    #RADIAL SECTION
    edge_points_one=[]
    #ff=0
    
    theta_index=0
    if (len(r_mask)==0):
        rmask_exists=False
        fun_iter=10
    else:
        rmask_exists=True
        fun_iter=5


    time_0_c=time.time()
    
    
    sections_list=[]
    
    for theta in arange(0,globs.two_pi,theta_step):
          
          section=radial_section(theta,f,x_dim,y_dim)
          sections_list.append(section)
          
          #LIMITS FOR CONTOUR RECOGNITION
          
          if (rmask_exists):

              if (decreasing==False):
                  
                  l_limit=int(max([0,r_mask[theta_index]-r_offset]))
                  h_limit=min([r_mask[theta_index]+r_offset,len(section)])
              else:
                  l_limit=int(max([0,r_mask[theta_index]-r_offset]))
                  h_limit=min([r_mask[theta_index]+globs.interdrift,len(section)])
              

              if (l_limit>h_limit):
                  print "LIMIT ERROR",l_limit,h_limit,r_mask[theta_index],len(section)
                  
                  l_limit=int(min_radius)
                  
          else:
              l_limit=int(min_radius)
              h_limit=len(section)
          
              
          if (ff==0):
              maskin_used=maskin
              maskout_used=maskout
              #ff=1
          else:
              
              maskin_used=maskins
              maskout_used=maskouts
              #ff=0

             
          if (len(maskin_used)>l_limit):
              maskin_used=maskin[len(maskin_used)-l_limit:]
          #print len(maskin_used),l_limit
          function=lambda x: edge_contrast(section,x,maskin_used,maskout_used)
          edge_radius = fminbound(function,l_limit,h_limit,xtol=1,maxfun=fun_iter,disp=0)
          #(x1,x2,n) = optMinGold(function, l_limit,h_limit,maxit=fun_iter)
          #print theta,edge_radius,function(edge_radius)
          edge_points_one.append([theta,edge_radius,0,0])
	  #edge_points_one.append([theta,x1,0,0])
          theta_index=theta_index+1
          
    #print "time of rec",-(time_0_c-time.time())     
    
    #ELIMINATE BY DRIFT
    #edge_points_one: estimated_edge points
    #edge_points_two: estimated_edge points starting from longest cluster
    #edge_points_three: estimated_edge points starting from longest cluster with elimination of out-of-drift range 
          

    time_other=time.time()
    old=edge_points_one[0]

    agreement_index=0
    cluster_index=0
    for point in edge_points_one: 
        
        if (abs(point[1]-old[1])<min_drift):
            agreement_index=agreement_index+1
        else:
            agreement_index=0
            cluster_index=cluster_index+1
        point[2]=agreement_index
        point[3]=cluster_index
        #print cluster_index,agreement_index
        old=point

    #find max cluster
    #print "a",time.time()-ts
    edge_points_one=numpy.array(edge_points_one)
    max_position=maximum_position(edge_points_one[:,2])[0]
    #print max_position,edge_points_one[max_position]
    cluster_name=edge_points_one[max_position][3]

    i=0
    for row in edge_points_one[:,2:]:
        if ((row[0]==0)&(row[1]==cluster_name)):
            
            break
        i=i+1
    #print edge_points_one[i]
    #print max_position,i

    #start edge from max cluster

    edge_points_two=join_array(edge_points_one[i:],edge_points_one[:i])
    edge_points_two=numpy.array(edge_points_two)
    edge_points_two_prime=join_array(edge_points_one[max_position+1:],edge_points_one[:max_position+1])
    edge_points_two_prime=numpy.array(edge_points_two_prime)
    #edge_points_one=numpy.concatenate(edge_points_one[i:],edge_points_one[:i])
    del edge_points_one

    #print "b",time.time()-ts
    #eliminate when too much drift

    edge_points_three=[]

    begin=edge_points_two[0][1]
    begin_theta=edge_points_two[0][0]
    #print begin_theta
    r_for_drift=[]
    indrift_flag=0
    for row in edge_points_two:
        
        theta=row[0]
        r=row[1]
        agreement_index=row[2]
        [x,y]=cartesian(r,theta)
        #r_drift=r
        
        if (indrift_flag<25):
            r_for_drift.append(r)
            begin=mean(r_for_drift)
        indrift_flag=indrift_flag+1
        
        #im.putpixel((int(x+x_dim_half),int(y+y_dim_half)),(255,255,0))\
        drift_margin=((drift_coefficient*min([abs(begin_theta-theta),abs(begin_theta-theta-globs.two_pi)]))+drift_constant)
        try:
            im.putpixel(add([x_dim_half,y_dim_half],cartesian(begin+drift_margin,theta)),(100,25,0))
            im.putpixel(add([x_dim_half,y_dim_half],cartesian(begin-drift_margin,theta)),(100,25,0))
            im.putpixel(add([x_dim_half,y_dim_half],cartesian(begin,theta)),(100,100,0))
        except(IndexError):
            errmsg=1
            #print "oor"
        if (((abs(begin-r))<drift_margin)):
            #print begin_theta-theta
            #print begin_theta,theta
            try:
                im.putpixel((int(x+x_dim_half),int(y+y_dim_half)),(255,255,0))
            except(IndexError):
                errmsg=1
                #print "oor"
            #begin=r
            begin_theta=theta
            edge_points_three.append(row)
            if (indrift_flag>24):
                r_for_drift.append(r)
                r_for_drift.pop(0)
            
            r_drift=mean(r_for_drift)
            #print r_drift,r
            begin=r_drift
        else:
            try:
                im.putpixel((int(x+x_dim_half),int(y+y_dim_half)),(255,0,0))
            except(IndexError):
                errmsg=1
    del edge_points_two
    
    begin=edge_points_two_prime[-1][1]
    begin_theta=edge_points_two_prime[-1][0]
    #print begin_theta
    r_for_drift=[]
    indrift_flag=0
    
    for k in range(len(edge_points_two_prime),0,-1):
        row=edge_points_two_prime[k-1]
    
   
        theta=row[0]
        r=row[1]
        agreement_index=row[2]
        [x,y]=cartesian(r,theta)
        #r_drift=r
        
        if (indrift_flag<25):
            r_for_drift.append(r)
            begin=mean(r_for_drift)
        indrift_flag=indrift_flag+1
        
        #im.putpixel((int(x+x_dim_half),int(y+y_dim_half)),(255,255,0))\
        drift_margin=((drift_coefficient*min([abs(begin_theta-theta),abs(begin_theta-theta-globs.two_pi)]))+drift_constant)
        try:
            im.putpixel(add([x_dim_half,y_dim_half],cartesian(begin+drift_margin,theta)),(100,25,0))
            im.putpixel(add([x_dim_half,y_dim_half],cartesian(begin-drift_margin,theta)),(100,25,0))
            im.putpixel(add([x_dim_half,y_dim_half],cartesian(begin,theta)),(100,100,0))
        except(IndexError):
            errmsg=1
            #print "oor"
        if (((abs(begin-r))<drift_margin)):
            #print begin_theta-theta
            #print begin_theta,theta
            if(if_contains(edge_points_three,row)==0):
                try:
                    im.putpixel((int(x+x_dim_half),int(y+y_dim_half)),(5,155,0))
                except(IndexError):
                    errmsg=1
                    #print "oor"
                edge_points_three.append(row)
            #begin=r
            begin_theta=theta
            #
            
            
            if (indrift_flag>24):
                r_for_drift.append(r)
                r_for_drift.pop(0)
            
            r_drift=mean(r_for_drift)
            #print r_drift,r
            begin=r_drift

    #print edge_points_one[0]
    #print numpy.max(edge_points_one[:,3])
    #im=Image.blend("RGB", background, 0.2)
    del edge_points_two_prime
    edge_points_three=numpy.array(edge_points_three)


    
    #print "c",time.time()-ts
    #now Fourier analysis:
    r_vals=[]
    x_coll=edge_points_three[:,0]
    y_coll=numpy.array(edge_points_three[:,1])
    cof=[]

    for k in range(1,fourier_max):
        #print k
        function=lambda x: math.sin(k*x)
        cof.append(map(function,x_coll))
        function=lambda x: math.cos(k*x)
        cof.append(map(function,x_coll))

    cof=(numpy.array(cof)).transpose()
    #print cof.shape
    
    mls = ols.ols(y_coll,cof)
    
    fourier_coefficient=mls.b

    #a_coeff=[]
    #b_coeff=[]
    #for k in range(0,fourier_max):
         #a_coeff.append(fourier_component(edge_points_three,"cos",k))
         #b_coeff.append(fourier_component(edge_points_three,"sin",k))

    edge_points_four=[]
    recognition_goodness=[]
    recognition_percantage=[]
    xy_vals=[]
    
    #theta_ind=0
    for theta in arange(0,globs.two_pi,theta_step):
        #r_val=0.5*a_coeff[0]
        r_val=fourier_coefficient[0]
        #print a_coeff[0]
        for k in range(1,fourier_max):
            #print k
            r_val=r_val+fourier_coefficient[2*k-1]*math.sin(theta*k)
            r_val=r_val+fourier_coefficient[2*k]*math.cos(theta*k)
            
            #r_val=r_val+a_coeff[k]*cos(theta*k)
            #r_val=r_val+b_coeff[k]*sin(theta*k)
        #print r_val    
        edge_points_four.append([theta,r_val])
        [x,y]=cartesian(r_val,theta)
        #WRONG FOURIER CORRECTION 
        if (r_val<0):
            r_val=1
        r_valint=int(r_val)
        r_vals.append(r_valint)
        xy_vals.append([x,y])
        #print x,y
        t_0=time.time()
        #section=radial_section(theta,f,x_dim,y_dim)
        section=sections_list.pop(0)
        #theta_ind=theta_ind+1
        t_1=time.time()-t_0
        time_cum=time_cum+t_1 
        #ERROR HANDLING HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        
        out_brightness=section[r_valint:r_valint+goodness_out_len]
        in_brightness=section[r_valint-goodness_out_in:r_valint]
        goodness=mean(in_brightness)/mean(out_brightness)
        if (goodness>1.5):
            recognition_percantage.append(1)
            try:
                im.putpixel((int(x+x_dim_half),int(y+y_dim_half)),(100,100,250))
                
            except(IndexError):
                errmsg=1
        else:
            recognition_percantage.append(0)
            try:
                im.putpixel((int(x+x_dim_half),int(y+y_dim_half)),(100,250,100))
            except(IndexError):
                errmsg=1
    
    
    xy_vals=numpy.array(xy_vals)
    
    #for xy in xy_vals:
    #    print xy
    r_vals_2=map(float,multiply(r_vals,r_vals))
    #r_shifted=shift_contour(r_vals,(20,0))
    
    denominator=float(sum(r_vals_2))
    #print denominator
    #print sum(multiply(xy_vals[:,0],r_vals_2))
    x_orig=(2/3.0)*sum(multiply(xy_vals[:,0],r_vals_2)/denominator)
    y_orig=(2/3.0)*sum(multiply(xy_vals[:,1],r_vals_2)/denominator)
    self.percantage_recognized=mean(recognition_percantage)
    print "ppr",self.percantage_recognized
    if (len(r_mask)!=0):
    
        for i in range(len(r_mask)):
            #print r_mask[i]
            x,y=cartesian(r_mask[i],i*theta_step)
            try:
                   im.putpixel((int(x_dim_half+x),int(y+y_dim_half)),(100,0,150))
            except(IndexError):
                    errmsg=1
                    #print "ppp",x_dim_half+x,y+y_dim_half,x_dim_half,y_dim_half
        #(x-x_orig)
    #print "time",time_cum
    self.fourier_coefficient=fourier_coefficient

    draw = ImageDraw.Draw(im)
    draw.text((10,10), str(round(self.percantage_recognized,2)), fill="yellow",font=font2)

                        
    self.center=[x_orig,y_orig]
    #print (self.center[0]+x_dim_half,self.center[1]+y_dim_half)
    draw.ellipse((self.center[0]+x_dim_half-delta,self.center[1]+y_dim_half-delta,self.center[0]+x_dim_half+delta,self.center[1]+y_dim_half+delta),outline=(255,0,19))
    draw.ellipse((old_center[0]+x_dim_half-delta,old_center[1]+y_dim_half-delta,old_center[0]+x_dim_half+delta,old_center[1]+y_dim_half+delta),outline=(0,200,19))
    #print "d",time.time()-ts
    #print self.center
    #draw.text((self.center[0]+x_dim_half,self.center[1]+y_dim_half), str("x"), fill="blue",font=font2)
    #draw.text((old_center[0]+x_dim_half,old_center[1]+y_dim_half), str("x"), fill="red",font=font2)
    #draw.text((x_dim_half,y_dim_half), str("o"), fill="violet",font=font2)
    im.putpixel((int(x_dim_half),int(y_dim_half)),(10,0,250))
    self.image=im
    self.area=denominator
    self.r_contour=r_vals
    #print "time other",-(time_other-time.time())
    print "time tot",-(time_start-time.time())


