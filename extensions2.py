def saveTiffMultipageFromSeq(arrseq, fn, rescaleSeqTo8bit=False, rgbOrder="rgba", **params):
    """
    arrseq can be an iterator that yield 2D(grey) or 3D(color) image

    extension to PIL save TIFF

    if rescaleSeqTo8bit: scale each section (separately!) to 0..255
        (ratios between colors are unchanged)
    **params is directly forwarded to PIL save function
    """
#     if arr.ndim == 4:
#         if arr.shape[1] not in (1,2,3,4):
#             raise ValueError, "can save 4d arrays (color) only with second dim of len 1..4 (RG[B[A]])"
#     elif arr.ndim != 3:
#         raise ValueError, "can only save 3d (grey) or 4d (color) arrays"

    fp = open(fn, 'w+b')

    ifd_offsets=[]

#     if rescaleTo8bit:
#         mi,ma = float(arr.min()), float(arr.max())
#         ra = ma-mi

    params["_debug_multipage"] = True
    for z,a in enumerate(arrseq):
        if rescaleSeqTo8bit:
            mi,ma = float(a.min()), float(a.max())
            ra = ma-mi
            a=(a-mi)*255./ra
            ii = array2image(a.astype(N.uint8), rgbOrder=rgbOrder)
        else:
            
            #ii = array2image(a, rgbOrder=rgbOrder)
            ii = a

        fp.seek(0,2) # go to end of file
        if z==0:
            # ref. PIL  TiffImagePlugin
            # PIL always starts the first IFD at offset 8
            ifdOffset = 8
        else:
            ifdOffset = fp.tell()

        ii.save(fp, format="TIFF", **params)
        
        if z>0: # correct "next" entry of previous ifd -- connect !
            ifdo = ifd_offsets[-1]
            fp.seek(ifdo)
            ifdLength = ii._debug_multipage.i16(fp.read(2))
            fp.seek(ifdLength*12,1) # go to "next" field near end of ifd
            fp.write(ii._debug_multipage.o32( ifdOffset ))

        ifd_offsets.append(ifdOffset)
    fp.close()

