# -*- coding: cp1250 -*-
from Imports import *
from module_a04 import main as prerecognition
from module_b06 import main as contour
from module_b06 import polar,cartesian
from module_a04 import gaussian_grid,GAUSSIAN
from cutting_module import main as cutting
import globs


class slow_contour(threading.Thread):
        def __init__(self,*args):
            threading.Thread.__init__(self)
            self.args=args
            self.out=False
            
        def run(self):
                try:
                  out=contour(*self.args)
                  state=True
                except(IndexError,ValueError):
                  out=[]
                  state=False
                self.out=out
                self.state=state

two_pi=2*pi
theta_step=globs.theta_step

class MyWriter:

    def __init__(self, stdout, filename):
        self.stdout = stdout
        self.logfile = file(filename, 'w')

    def write(self, text):
        self.stdout.write(text)
        self.logfile.write(text)

    def close(self):
        print "closing"
        #self.stdout.close()
        self.logfile.close()

def first_index(seq,element):
    for i in range(len(seq)):
        
        if (((seq[i-1])<element)&(element<=(seq[i]))):
            break
    return (i)

def last_decreasing(seq,value):
  #print "DECR1",seq,value
  seq_len=len(seq)
  seq_last=seq[-1]
  k_last=seq_len
  for k in range (1,seq_len):
    item=seq[seq_len-k-1]
    #print "ITEM",item,k
    if (item>seq_last):
      seq_last=item
    else:
      k_last=k      
      break
  #print "DECR2",k_last,(k_last>=value)
  return((k_last>=value))         

def shift_contour(contour,shift_offset):
    new_contour=[]
    k=0
    temp_contour_r=[]
    temp_contour_theta=[]
    for theta in arange(0,two_pi,theta_step):
        r=contour[k]
        x,y=cartesian(r,theta)
        x_new=x-shift_offset[0]
        y_new=y-shift_offset[1]
        r_new,theta_new=polar(x_new,y_new)
        temp_contour_r.append(r_new)
        temp_contour_theta.append(theta_new)
        #print r,r_new
        k=k+1
    #print temp_contour_theta
    for theta in arange(0,two_pi,theta_step):
        index=first_index(temp_contour_theta,theta)
        #print theta,index,temp_contour_r[index]
        new_contour.append(temp_contour_r[index])
    return(new_contour)
        

def frame_cutting(plane,origin,block_range,offset=[0,0]):
    y_m=origin[0]+offset[1]
    x_m=origin[1]+offset[0]
    print "offset", offset
    print "origin",x_m,y_m
    cut=plane[y_m-block_range:y_m+block_range,x_m-block_range:x_m+block_range]
    new_origin=(y_m,x_m)
    return (cut,new_origin)
   

def remove_check(origin,points_list,edge,margin):
   p_len=len(points_list)
   for i in range(p_len-1,-1,-1):
      point=points_list[i][0]
      relative_coords=subtract(point,origin)
      r,theta=polar(*relative_coords)
      theta_index=int(theta/globs.theta_step)
      r_edge=edge[theta_index]
      #print r,r_edge
      if (r<r_edge+margin):
         print "----"
         print "removed"
         print points_list.pop(i)
         #print r_edge,r
         print "****"
         
      

def list_size(lst):
   elements=0
   for item in lst:
       if (type(item)==ListType):
           elements=elements+list_size(item)
       else:
           elements=elements+1
   return (elements)
            
        

def show_image(im,text=None):
    #img=scipy.misc.toimage(cut,  mode="L")
    #im=img.convert("RGB")
    global im_counter
    global time_start
    #tkIm = ImageTk.PhotoImage(im)
    #label_image = tk.Label(root, image=tkIm)
    #label_image.place(x=0,y=0,width=im.size[0],height=im.size[1])
    if (text!=None):
      pygame.display.set_caption(text)
        #label_text = tk.Label(root, text=text)
        #label_text.place(x=im.size[0]+20,height=im.size[1]/2)
        #label_text.config(bg='black', fg='yellow')
    if (globs.test==True):    
            print str(im_counter)+"_"+str(current_plane+1000)[1:]+".tif"+" saved"
            im.save("C:\\Users\\aaron\\Desktop\\segmentation\\out\\"+str(im_counter)+"_"+str(current_plane+1000)[1:]+".tif","TIFF")
    print "nuclei recognized:",len(nuclei_list) 
    data = im.tostring()
    size=im.size
    picture = pygame.image.fromstring(data, size, pg_mode)
    screen.blit(picture, (block_range/2.0, block_range/2.0))
    pygame.display.flip()
    print "TIME",time_start-time.time()
    time_start=time.time()
    


def hotpoints_max (hotpoints_list):
    value=-100
    plane_index=0
    for plane in hotpoints_list:
        point_index=0
        for point in plane:
            brightness_ratio=point[1][0]
            
            if (brightness_ratio>value):
                #print brightness_ratio,value
                value=brightness_ratio
                c_plane=plane_index
                c_point=point_index
            point_index=point_index+1
            
        plane_index=plane_index+1
    print "max_value",value
    return([c_plane,c_point])


            
 

#MAIN PROGRAM
global time_start

root=tk.Tk()
root.withdraw()

initdirpath=globs.configpath+"\\init_dir.dat"
try:
    parms_file = open(initdirpath, 'rb')
    dir_name= pickle.load(parms_file)
    parms_file.close()
except(IOError,EOFError):
    dir_name="c:\\"
stack=askfn(initialdir=dir_name,title="Open Image File",filetypes=[('TIFF files', '.tif')])
time_start=0
dir_name,file_name=os.path.split(stack)
dir_name=os.path.normpath(dir_name)
file_beg,file_ext=os.path.splitext(file_name)
output_path=dir_name+"\\"+file_beg+".msk"
log_file_name=dir_name+"\\"+file_beg+".log"
err_file_name=dir_name+"\\"+file_beg+".err"
fsock = open(err_file_name, 'w')
#sys.stderrsys.stderr = fsock
writer = MyWriter(sys.stdout, log_file_name)
stdouttemp = sys.stdout
sys.stdout = writer

pg_mode = "RGB"
nuclei_list=[]
   


parms_file = open(initdirpath, 'wb')
#print stack
stack=os.path.normpath(stack)
print initdirpath,dir_name
pickle.dump(dir_name,parms_file,2)
parms_file.close()
block_range=int(globs.block_range)
max_depth_planes=globs.max_depth_planes
head_percentage=globs.head_percentage
acceptance_percentage=globs.acceptance_percentage
scale=globs.scale
use_booster=globs.use_booster
im_counter=0



im =Image.open(stack)
#root.geometry('840x840+0+0')
#im = im.filter(ImageFilter.EDGE_ENHANCE)
#im = im.filter(GAUSSIAN)
#a=scipy.misc.fromimage(im,flatten=True)
#result=main(a)
#im=result.image
#tkIm = ImageTk.PhotoImage(im)
#label_image = tk.Label(root, image=tkIm)
#label_image.place(x=0,y=0,width=im.size[0],height=im.size[1])
#label_image.pack()
#root.update()
#result=main(a,result.r_contour)
#im=result.image

if (im.mode=="P"):
    tkMessageBox.showwarning("Indexeed colour image stack can not be accepted. Convert the images to RGB colour or grayscale!" )
    os._exit(0)
if (im.mode=="RGB"):
    tkMessageBox.showwarning("Colour image will be flatten before processing!" )
  


    
#im=im.convert("RGB")
#PRODUCEE THE LIST WITH IMAGES

hotpoints=[]
image_sequence=[]
smallimage_sequence=[]
#th = threading.Thread(target=th2)
#th.start()
print "OPENING IMAGES..."
frame_index = 0
try:
  while 1:
    im.seek(frame_index)
    print frame_index
    frame_index = frame_index + 1
    if (im!=None):
        if (im.mode!="L"):
            frame=im.convert("L")
        else:
            frame=im
        frame_filtered = frame.filter(GAUSSIAN)
	im_small=frame_filtered.resize(map(int,multiply(frame.size,scale)),Image.BICUBIC)
	a=numpy.array(frame_filtered)
	del frame_filtered
        a_small=scipy.misc.fromimage(im_small,flatten=True)
        
        image_sequence.append(a)
	del a 
        smallimage_sequence.append(a_small)
	gc.collect()
	
	
	#q.put(frame_filtered)
        #tkIm = ImageTk.PhotoImage(frame_filtered)
        #label_image = tk.Label(root, image=tkIm)
        #label_image.place(x=0,y=0,width=im.size[0],height=im.size[1])
        #root.update()
        #time.sleep(15)
except EOFError:
   print "eof"
   im.seek(0)
   pass

del im
del im_small
last_frame_index=frame_index-1

#ROUGH POSITION
pygame.init()
print "FINDING ROUGH POSIITION..."
for a,a_small in zip(image_sequence,smallimage_sequence):
    
    current_plane=prerecognition(a,a_small,False)
    print "nr of points",len(current_plane.hotpoints)
    hotpoints.append(current_plane.hotpoints)
#print len(hotpoints)
#pkl_file = open("c:\\hotpoints.dat", 'rb')
#hotpoints= pickle.load(pkl_file)
#pickle.dump(hotpoints,pkl_file,2)

print len(hotpoints),len(image_sequence)
print list_size(hotpoints)
#ind=hotpoints_max(hotpoints)
#print ind
#print hotpoints[ind[0]][ind[1]]
#print hotpoints[ind[0]].pop(ind[1])
del a_small

#CONTOURS RECOGNITION

screen=pygame.display.set_mode((3*block_range,3*block_range))
pygame.display.set_caption("initial analysis")
pygame.init()



hpinit_size=list_size(hotpoints)


while((list_size(hotpoints)>0)):
    area_list=[]    
    list_of_masks=[]
    im_counter=im_counter+1
    ind=hotpoints_max(hotpoints)
    
    current_plane=ind[0]
    #plane number,point_number
    #print hotpoints[ind[0]][ind[1]]
    point=hotpoints[ind[0]].pop(ind[1])
    origin=point[0]
    
    print "starting point",origin
    print "number of points",list_size(hotpoints)  
    frame=image_sequence[ind[0]]
    print "new_frame",origin

    try:
       cut,origin=frame_cutting(frame,origin,block_range)
       background=slow_contour(cut)
       background.start()

       while background.isAlive():
         time.sleep(0.1)
         pygame.event.pump()
        
                
       background.join()
       if (background.state==True):
         
         result=background.out
       else:
         continue
    except(ValueError,IndexError):
       print "block  too small"
       continue
       
    if (globs.test==True): 
            show_image(result.image,text="head plane")
    try:
       cut,origin=frame_cutting(frame,origin,block_range,offset=result.center)
       background=slow_contour(cut,shift_contour(result.r_contour,result.center))
       background.start()

       while background.isAlive():
          time.sleep(0.1)
          pygame.event.pump()
         
                
       background.join()
       if (background.state==True):  
         result=background.out
       else:
         continue
       
    except(ValueError,IndexError):
       print "block  too small"
       continue
    if (result.percantage_recognized<acceptance_percentage):
        continue
    if (result.percantage_recognized<head_percentage):
       background=slow_contour(cut,result.r_contour)
       background.start()

       while background.isAlive():
         time.sleep(0.1)
         pygame.event.pump()
         
                
       background.join()
       if (background.state==True):  
         result=background.out
        
    print "B",origin
    head_plane_contour=result.r_contour[:]
    head_plane_offset=result.center[:]
    head_plane_origin=origin[:]
    im_out=result.image
    #img=scipy.misc.toimage(cut,  mode="L")
    #im=img.convert("RGB")
    if ((result.percantage_recognized<head_percentage)&(point[1][0]>0)):
            if (globs.test==True):     
                    show_image(im_out,text="UNRECOGNIZED")
            if ((result.percantage_recognized>(0.5*(acceptance_percentage+1.0)))&(use_booster==True)):
                hotpoints[ind[0]].append([numpy.array(origin),numpy.array([result.percantage_recognized-head_percentage,0])])
                print "appended",[origin,numpy.array([result.percantage_recognized-head_percentage,0])]
            continue

    
    show_image(im_out,text=None)
    mask=[current_plane,origin,result.r_contour,result.percantage_recognized,result.area]
    list_of_masks.append(mask)
    area_list.append(result.area)
    print "IN APPEND",len(area_list)
    
    #GOING UP
    up_outcome="exhausted"
    planes_done=0
    
    for plane_shift in range(1,last_frame_index):
    #for plane_shift in range(1,max_depth_planes):


         current_plane=ind[0]+plane_shift

         if ((current_plane)>last_frame_index):
              up_outcome="picture_end"
              break
              
         frame=image_sequence[current_plane]
         cut,origin=frame_cutting(frame,origin,block_range,offset=result.center)
          
         try:
             
             background=slow_contour(cut,shift_contour(result.r_contour,result.center),last_decreasing(area_list,globs.decrease_def))
             background.start()

             while background.isAlive():
                 time.sleep(0.1)
                 pygame.event.pump()
                 
                        
             background.join()
             if (background.state==True):  
               result=background.out
             else:
               break
         except(ValueError,IndexError):
            print "out of bounds"
            break
         area_list.append(result.area)
         print "UP APPEND",len(area_list)
         remove_check(origin,hotpoints[current_plane],result.r_contour,globs.remove_margin)

         if (result.percantage_recognized<acceptance_percentage):
            if (plane_shift<=max_depth_planes):
                   up_outcome="no_recog"
            break
            
         progress_string="SEGMENTATION PROGRESS: "+str(int(100.0*(1.0-double(list_size(hotpoints))/hpinit_size)))  +" % done   "
         show_image(result.image,text=progress_string)         
         mask=[current_plane,origin,result.r_contour,result.percantage_recognized,result.area]
         list_of_masks.append(mask)
         planes_done=planes_done+1
         
    #GOING DOWN
    result.r_contour=head_plane_contour
    result.center=head_plane_offset
    origin=head_plane_origin
    down_outcome="exhausted"
    area_list.reverse() 

    for plane_shift in range(1,last_frame_index):
    #for plane_shift in range(1,max([1,max_depth_planes-planes_done])):
              
        current_plane=ind[0]-plane_shift

        if ((current_plane)<0):
              down_outcome="picture_end"
              break

        frame=image_sequence[current_plane]
        
        cut,origin=frame_cutting(frame,origin,block_range,offset=result.center)
  
        try:
             
             background=slow_contour(cut,shift_contour(result.r_contour,result.center),last_decreasing(area_list,globs.decrease_def))
             background.start()

             while background.isAlive():
                 time.sleep(0.1)
                 pygame.event.pump()
                 
                        
             background.join()
             if (background.state==True):  
               result=background.out
             else:
               break
        except(ValueError,IndexError):
            print "out of bounds"
            break
        remove_check(origin,hotpoints[current_plane],result.r_contour,globs.remove_margin)
        area_list.append(result.area)
        print "UP APPEND",len(area_list)
        if (result.percantage_recognized<acceptance_percentage):
            if (plane_shift<=max([1,max_depth_planes-planes_done])):
                      down_outcome="no_recog"
            break

        progress_string="SEGMENTATION PROGRESS: "+str(int(100.0*(1.0-double(list_size(hotpoints))/hpinit_size)))  +" % done   "
    
        show_image(result.image,text=progress_string)         
        mask=[current_plane,origin,result.r_contour,result.percantage_recognized,result.area]
        list_of_masks.append(mask)
        list_of_masks.sort(lambda x, y: cmp(x[0],y[0]),reverse=0)


    #POSTPROCESSING
    nucleus=[im_counter,up_outcome,down_outcome,list_of_masks]
    if ((len(list_of_masks))>globs.min_depth_planes):
        nuclei_list.append(nucleus)


nuclei_list.sort(lambda x, y: cmp(x[3][0][0],y[3][0][0]),reverse=0)

del image_sequence
pygame.display.quit()

write_file1=open(output_path, "wb")
pickle.dump(nuclei_list,write_file1,2)
write_file1.close()

print "finished after",time.time()-time_start
#main(stack,mask,make_description=True)
del nuclei_list


cutting(stack,output_path,make_description=True)

writer.close()
sys.stdout = stdouttemp


