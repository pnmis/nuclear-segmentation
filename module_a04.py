# -*- coding: cp1250 -*-
import globs
from Imports import *

def gaussian_grid(size = 5):
    """
    Create a square grid of integers of gaussian shape
    e.g. gaussian_grid() returns
    array([[ 1,  4,  7,  4,  1],
           [ 4, 20, 33, 20,  4],
           [ 7, 33, 55, 33,  7],
           [ 4, 20, 33, 20,  4],
           [ 1,  4,  7,  4,  1]])
    """
    m = size/2
    n = m+1  # remember python is 'upto' n in the range below
    x, y = mgrid[-m:n,-m:n]
    # multiply by a factor to get 1 in the corner of the grid
    # ie for a 5x5 grid   fac*exp(-0.5*(2**2 + 2**2)) = 1
    fac = exp(m**2)
    g = fac*exp(-0.5*(x**2 + y**2))
    return g.round().astype(int)

class GAUSSIAN(ImageFilter.BuiltinFilter):
    name = "Gaussian"
    gg = gaussian_grid().flatten().tolist()
    filterargs = (5,5), sum(gg), 0, tuple(gg)

    

class scan_grid(threading.Thread):
        def __init__(self,a,spacing,block_dim):
            threading.Thread.__init__(self)
            self.a=a
            self.spacing=spacing
            self.block_dim=block_dim
            self.out=[]
            
        def run(self):
                
                grid_1=get_grid(self.a,self.spacing,scale*100,scale*globs.min_diameter)
                grid_2=get_grid(self.a,self.spacing,scale*100,scale*globs.med_diameter)
                grid_3=get_grid(self.a,self.spacing,scale*100,scale*globs.max_diameter)
                
                hotpoints_list=grid_1.list+grid_2.list+grid_3.list
                hotpoints_list.sort(lambda x, y: cmp(x[1][0],y[1][0]),reverse=1)
                self.out=hotpoints_list

class get_grid:
        
  def __init__(self,a,spacing,block_dim,a_radius):
        x_dim=len(a)
        y_dim=len(a[0])
        xs_max=int((x_dim-1)/spacing)
        ys_max=int((y_dim-1)/spacing)
        print  x_dim,y_dim
        sites_matrix=numpy.zeros((xs_max+1,ys_max+1,2))
        edge_margin=int(block_dim/spacing)+1
        x_d=int(block_dim*2+1)
        x_med=(x_d+1)/2
        weigth_matrix_in=numpy.zeros((x_d,x_d))
        weigth_matrix_out=numpy.zeros((x_d,x_d))

        print "point a,radius:",a_radius
        beg_time=time.time()
        for i in range(x_d):
                for j in range(x_d):
                    dist=sqrt(pow((i-x_med),2)+pow((j-x_med),2))
                    weigth_matrix_in[i,j]=s_step(dist,a_radius)
                    #weigth_matrix_in[i,j]=gaussian([i,j],(x_med,x_med),a_radius)
                    #weigth_matrix_out[i,j]=s_step(dist,b_radius)-s_step(dist,a_radius)
                    weigth_matrix_out[i,j]=1
                    #weigth_matrix_out[i,j]=gaussian([i,j],(x_med,x_med),2*a_radius)
        print "point b",time.time()-beg_time
        weigth_matrix_out=weigth_matrix_out-weigth_matrix_in
        weigth_matrix_in=numpy.multiply(1.0/sum(weigth_matrix_in),weigth_matrix_in)
        weigth_matrix_out=numpy.multiply(1.0/sum(weigth_matrix_out),weigth_matrix_out)
        
        print "point c",time.time()-beg_time
        print edge_margin,sites_matrix.shape[0]-edge_margin      
        for i in range(edge_margin,sites_matrix.shape[0]-edge_margin):
            for j in range(edge_margin,sites_matrix.shape[1]-edge_margin):
                x=i*spacing
                y=j*spacing
               
                block=a[x-block_dim:x+block_dim+1,y-block_dim:y+block_dim+1]
                
                sites_matrix[i,j]=weigthed_sum(block,weigth_matrix_in,weigth_matrix_out)
        print "point d",time.time()-beg_time
        hotpoints_list=[]
        for i in range(sites_matrix.shape[0]):
            for j in range(sites_matrix.shape[1]):
                if (((sites_matrix[i,j][1])>include_brightness)&((sites_matrix[i,j][0])>rough_level)):
                        hotpoints_list.append([multiply((1.0/scale)*spacing,[i,j]),sites_matrix[i,j]])
        print "point e",time.time()-beg_time
        self.list=hotpoints_list
        self.matrix=sites_matrix           
          
def stamps((x_dim,y_dim)):
        #x_dim,y_dim]=block.shape
        stamp=[]
        for i in range(6):
                stamp.append(numpy.zeros((x_dim,y_dim)))
        
        x_med=(x_dim+1)/2
        y_med=(y_dim+1)/2
        qxx=0
        qyy=0
        qxy=0
        qxx=0
        qyy=0
        qxy=0
        dpx=0
        dpy=0
        m=0
        
        for i in range(x_dim):
                for j in range(y_dim):
                        x=i-x_med
                        y=j-y_med
                        r2=pow(x,2)+pow(y,2)
                        correction=pow(r2+1,0.25)
                        g=s_step(r2,2500)
                        
                        stamp[1][i,j]=g*x/correction
                        stamp[2][i,j]=g*y/correction
                        #new_b[i,j]=block[i,j]#*x/sqrt(r2)
                        stamp[0][i,j]=1
                        stamp[3][i,j]=g*(2*x*y)/pow(correction,4)
                        stamp[4][i,j]=g*(2*x*x-r2)/pow(correction,4)
                        stamp[5][i,j]=g*(2*y*y-r2)/pow(correction,4)
                        
                        #print (2*x*y)/correction,(2*x*x-r2)/correction,(2*y*y-r2)/correction
        
        #det=(qxx*qyy-pow(qxy,2))
        #dpm=sqrt(pow(dpx,2)+pow(dpy,2))/m
        #print m,dpx,dpy,dpm
        return(stamp)


def weigthed_sum(block,weigth_matrix_in,weigth_matrix_out):
   
    white=numpy.sum(numpy.multiply(block,weigth_matrix_in))
    black=numpy.sum(numpy.multiply(block,weigth_matrix_out))
    return [white/black,white]





def quadrupole(block,stamp):
        [x_dim,y_dim]=block.shape
        new_b=numpy.zeros((x_dim,y_dim))
        
        x_med=(x_dim+1)/2
        y_med=(y_dim+1)/2
        qxx=0
        qyy=0
        qxy=0
        qxx=0
        qyy=0
        qxy=0
        dpx=0
        dpy=0
        m=0
        #print x_dim,y_dim
        #for i in range(x_dim):
                #for j in range(y_dim):
                        #x=i-x_med
                        #y=j-y_med
                        #r2=pow(x,2)+pow(y,2)
                        #correction=pow(r2+1,0.25)
                        #g=s_step(r2,2500)
                        
                        #dpx=dpx+g*block[i,j]*x/correction
                        #dpy=dpy+g*block[i,j]*y/correction
                        #new_b[i,j]=block[i,j]#*x/sqrt(r2)
                        #m=m+g*block[i,j]
                        #qxy=qxy+g*block[i,j]*(2*x*y)/correction
                        #qxx=qxx+g*block[i,j]*(2*x*x-r2)/correction
                        #qyy=qyy+g*block[i,j]*(2*y*y-r2)/correction
                        
                        #print (2*x*y)/correction,(2*x*x-r2)/correction,(2*y*y-r2)/correction
        m=numpy.sum(numpy.multiply(stamp[0],block))
        dpx=numpy.sum(numpy.multiply(stamp[1],block))
        dpy=numpy.sum(numpy.multiply(stamp[2],block))
        qxy=numpy.sum(numpy.multiply(stamp[3],block))
        qxx=numpy.sum(numpy.multiply(stamp[4],block))
        qyy=numpy.sum(numpy.multiply(stamp[5],block))
        det=qxx*qyy/m-pow(qxy,2)/m
        #det=(qxx*qyy-pow(qxy,2))
        dpm=sqrt(pow(dpx,2)+pow(dpy,2))/m
        #print m,dpx,dpy,dpm
        return([dpm,det])
                 

class filter_bymoments:
        def __init__(self,a,hotpoints_reduceed):
                temp_list=[]
                for old in hotpoints_reduceed:   
                        [y_draw,x_draw]=old[0]
                        #newblock=im.crop((x_draw-block_dim,y_draw-block_dim,x_draw+block_dim,y_draw+block_dim))
			cim=a[y_draw-block_dim:y_draw+block_dim,x_draw-block_dim:x_draw+block_dim]
                        #cim=numpy.mat(scipy.misc.fromimage(newblock,flatten=True))
			try:
				q_m=quadrupole(cim,stamp)
				if ((abs(q_m[1])<globs.quadrupole_step)&(q_m[0]<globs.dipole_step)):
					temp_list.append(old)
			except (MemoryError):
					print "block too small"
                self.out=temp_list
     
            
class reduce_points:
        def __init__(self,hotpoints_list):
                
                hotpoints_reduceed=[]
                if ((len(hotpoints_reduceed))>0):
                    hotpoints_reduceed.append(hotpoints_list.pop(0))

                for old in hotpoints_list:

                    append_flag=0
                    for new in hotpoints_reduceed:
                        
                        if(((distance(old[0],new[0]))<globs.p_separation)|(old[1][0]<min_level)):
                            append_flag=1
                            break             
                                
                    if (append_flag==0):
                        
                        hotpoints_reduceed.append(old)
                self.out=hotpoints_reduceed

class interactive_momentsset:
        def __init__(self,img,hotpoints_list,size,scaled):
                
                
                breakflag=100
                while (breakflag>0):       


                        breakflag=100

                        hotpoints_reduceed=(reduce_points(hotpoints_list)).out

                        while(breakflag>1):
				temp_list=(filter_bymoments(img,hotpoints_reduceed)).out
                                im=img.convert("RGB")
                                draw = ImageDraw.Draw(im)
                                

                                for element in temp_list:
                                                [y_draw,x_draw]=element[0]

                                                draw.ellipse((x_draw-delta,y_draw-delta,x_draw+delta,y_draw+delta),outline=(255,0,19))
                                                draw.text((x_draw,y_draw), str(round(element[1][0],2)), fill="yellow",font=font)
                                                draw.text((10,10), str(globs.dipole_step), fill="green",font=font2)
                                                draw.text((10,50), str(globs.quadrupole_step), fill="green",font=font2)
                                                
                                
                                data = im.tostring()
                                picture = pygame.image.fromstring(data, size, mode)
                                scaled_picture=pygame.transform.scale(picture,scaled)
                                screen.blit(scaled_picture, (0, 0))
                                pygame.display.update()
                                        

                                breakflag=100
                                while(breakflag>2):
                                        events=pygame.event.get()
                                        for event in events:
                                                #print event.key
                                                print event.type
                                                if (event.type==KEYDOWN):
                                                        if (event.unicode=="+"):
                                                                        print "+"
                                                                        globs.quadrupole_step=globs.quadrupole_step-100
                                                                        print globs.quadrupole_step
                                                                        breakflag=2
                                                                        break
                                                        if (event.unicode=="-"):
                                                                        print "-"
                                                                        globs.quadrupole_step=globs.quadrupole_step+100
                                                                        print globs.quadrupole_step
                                                                        breakflag=2
                                                                        break
                                                        if (event.unicode==">"):
                                                                        print ">"
                                                                        globs.dipole_step=globs.dipole_step-0.3
                                                                        print 
                                                                        breakflag=2
                                                                        break
                                                        if (event.unicode=="<"):
                                                                        print ">"
                                                                        globs.dipole_step=globs.dipole_step+0.3
                                                                        print globs.dipole_step
                                                                        breakflag=2
                                                                        break
                                                        if (event.unicode=="s"):
                                                                        print ">"
                                                                        globs.p_separation=globs.p_separation-10
                                                                        breakflag=1
                                                                        draw.text((300,300), "redrawing...", fill="orange",font=font3)
                                                                        data = im.tostring()
                                                                        picture = pygame.image.fromstring(data, size, mode)
                                                                        scaled_picture=pygame.transform.scale(picture,scaled)
                                                                        screen.blit(scaled_picture, (0, 0))
                                                                        pygame.display.update()
                                                                        pygame.event.pump()
                                                                        break
                                                        if (event.unicode=="d"):
                                                                        print ">"
                                                                        globs.p_separation=globs.p_separation+10
                                                                        print globs.dipole_step
                                                                        draw.text((300,300), "redrawing...", fill="orange",font=font3)
                                                                        data = im.tostring()
                                                                        picture = pygame.image.fromstring(data, size, mode)
                                                                        breakflag=1
                                                                        scaled_picture=pygame.transform.scale(picture,scaled)
                                                                        screen.blit(scaled_picture, (0, 0))
                                                                        pygame.display.update()
                                                                        pygame.event.pump()
                                                                        break
                                                if (event.type==QUIT):
                                                        breakflag=0
                                                        break
                             


def gaussian(x,m,sigma):
        d=subtract(x,m)
        num=multiply(d,d)
        den=multiply(sigma,sigma)
        #factor=1.0/pow(sigma*sqrt(pi)*2,len(x))
        return(exp(-0.5*sum(divide(num,den))))

def l_step(x,step):
        if (abs(x)>step): r=1.0
        else: r=0
        return(r)

def s_step(x,step):
        if (abs(x)<step): r=1.0
        else: r=0
        return(r)

def distance(a,b):
    dist=sqrt(pow((a[0]-b[0]),2)+pow((a[1]-b[1]),2))
    return (dist)




class main:
     def __init__(self,a,a_small,interactive=False):
        a_small=a_small.astype(numpy.int) 
	a=a.astype(numpy.int) 
        
	print type(a[0][0]),type(a_small[0][0])
        global include_brightness
        global scale
        global rough_level
        global min_level
        global block_dim
        global stamp
        global delta
        global font
        global font2
        global font3
        global mode
        global screen
        #print min_level
        #size =im.size

        scale=globs.scale
        size=[a.shape[1],a.shape[0]]
        print "size",size
        scaled_size= multiply(size,scale)
        spacing=max(1,scale*globs.spacing)
        block_dim=globs.block_dim
        min_level=globs.min_level
        
        delta=8
        scaled=(600,600)
        mode = "RGB"
        

        font = ImageFont.truetype("arial.ttf", 30)
        font2 = ImageFont.truetype("arial.ttf", 30)
        font3 = ImageFont.truetype("arial.ttf", 75)
        #im =ImageOps.autocontrast(im, cutoff=3)
        #im = im.filter(ImageFilter.EDGE_ENHANCE_MORE)
        
        #im_small = im_small.filter(GAUSSIAN)
       


        #global parameters

        spacing=max(1,scale*globs.spacing)
        block_dim=globs.block_dim
        
        
        rough_level=globs.rough_level
        print "rough_level",rough_level
        rough_level=1
        stamp=stamps((int(2*globs.block_dim),int(2*globs.block_dim)))



        #image opening



        #a=multiply(4,a)
        f=numpy.mat(a)
        a_flattened=array(a).flat
        image_mean=scipy.mean(a_flattened)
        print image_mean
        #print scipy.ndimage.histogram(a_flattened, 0, image_mean, 10, labels = None, index = None)
        #returns RuntimeError when image black
        include_brightness=image_mean/2
        print include_brightness*60
	print type(a[0][0])
        img=scipy.misc.toimage(a,  mode="L")
        im=img.convert("RGB")

        
        data = im.tostring()
        screen=pygame.display.set_mode(scaled)
        pygame.display.set_caption("initial analysis")
        
        picture = pygame.image.fromstring(data, size, mode)
        scaled_picture=pygame.transform.scale(picture,scaled)
        screen.blit(scaled_picture, (0, 0))
        pygame.event.pump()
        pygame.display.flip()
        
        #im = im.filter(ImageFilter.MaxFilter(3))

        time_zero=time.time()
        #scanning the plane

        background = scan_grid(a_small,globs.spacing,0.4*globs.block_dim)
        background.start()

        while background.isAlive():
                s=pygame.event.get()
                for event in s:
                        #print event.key
                        current_event=event.type
                        
        background.join()
        hotpoints_list=background.out
        print "ilosc punktow", len(hotpoints_list)

        #interactive parameters set
        #time_end=time.time()
        #print time_end-time_zero

        if (interactive==True):
                interactive_momentsset(img,hotpoints_list,size,scaled)
                
        #pygame.display.quit()
        print "a",len(hotpoints_list)
        hotpoints_reduceed=(reduce_points(hotpoints_list)).out
        print "b",len(hotpoints_reduceed)
        temp_list=(filter_bymoments(a,hotpoints_reduceed)).out
        print "c",len(temp_list)
        self.hotpoints=temp_list
        self.matrix_image=a
        




